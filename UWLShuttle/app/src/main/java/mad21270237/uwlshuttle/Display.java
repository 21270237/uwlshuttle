package mad21270237.uwlshuttle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.*;
import java.text.ParseException;

public class Display extends AppCompatActivity {

    public int station, direction, x=-2;
    public String busS,timeDiff;
    public ProgressBar pr;

    public SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    public  Date bus, first, last;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        station=Holder.getStation();
        direction=Holder.getDirection();

        pr=(ProgressBar) findViewById(R.id.progressBar);

        TextView t = (TextView) findViewById(R.id.time);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        timeDiff= nextBus();

        if(timeDiff.length()>2){
            t.setTextSize(15);
        }else {
            pr.setProgress(Integer.parseInt(timeDiff));
            if(Integer.parseInt(timeDiff)==1){
                timeDiff+=" minute remaining";
            }else {
                timeDiff+= " minutes remaining";
            }
        }
        t.setText(timeDiff);
    }



    public String nextBus()  {
       String now = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
       //String now ="8:45"; //fictive time for testing

        //Paragon --> Ealing Broadway
        if(station==1){
            busS = "07:40";
            first = StringToDate(busS);
            last = StringToDate("18:25");

            if(minuteDifference(StringToDate(now), first)>0){
                return "First stop is at 7:40";
            }
            //the last four departures are not in the usual 15 minutes period
            if(minuteDifference(StringToDate(now), last)<0){
                return "Last four stops are at: 19:05, 19:45, 20:25, 21:45";
            }

            x= checkTime(busS, now);

        }



        //Little Ealing Lane
        if(station==2){

            //Little Ealing Lane --> Paragon
            if(direction==1){
                busS = "07:55";
                first = StringToDate(busS);
                last = StringToDate("18:55");

                if(minuteDifference(StringToDate(now), first)>0){
                    return "First departure is at 7:55";
                }

                //the last four departures are not in the usual 15 minutes period
                if(minuteDifference(StringToDate(now), last)<0){
                    return "Last 3 departures are at: 19:33, 20:15, 21:08";
                }

                x= checkTime(busS, now);
            }

            //Little Ealing Lane --> Ealing Broadway
            if(direction==2){
                busS = "07:47";
                first = StringToDate(busS);
                last = StringToDate("18:32");

                if(minuteDifference(StringToDate(now), first)>0){
                    return "First departure is at 7:47";
                }
                //the last four departures are not in the usual 15 minutes period
                if(minuteDifference(StringToDate(now), last)<0){
                    return "Last four departures are at: 19:12, 19:52, 20:32, 21:52";
                }

                x= checkTime(busS, now);
            }
        }

        //South Ealing
        if(station==3){
            //South Ealing -->> Paragon
            if(direction==1){
                    busS = "07:54";
                    first = StringToDate(busS);
                    last = StringToDate("18:54");

                    if(minuteDifference(StringToDate(now), first)>0){
                        return "First departure is at 7:54";
                    }

                    //the last four departures are not in the usual 15 minutes period
                    if(minuteDifference(StringToDate(now), last)<0){
                        return "Last 3 departures are at: 19:34, 20:14, 21:07";
                    }

                    x= checkTime(busS, now);
            }

            //South Ealing -->> Ealing Broadway
            if(direction==2){
                if(direction==2){
                    busS = "07:48";
                    first = StringToDate(busS);
                    last = StringToDate("18:33");

                    if(minuteDifference(StringToDate(now), first)>0){
                        return "First departure is at 7:48";
                    }
                    //the last four departures are not in the usual 15 minutes period
                    if(minuteDifference(StringToDate(now), last)<0){
                        return "Last four departures are at: 19:13, 19:53, 20:33, 21:53";
                    }

                    x= checkTime(busS, now);
                }

            }
        }

        //St Mary's
        if(station==4){
            //St Mary's -->> Paragon
            if(direction==1){
                busS = "07:52";
                first = StringToDate(busS);
                last = StringToDate("18:52");

                if(minuteDifference(StringToDate(now), first)>0){
                    return "First departure is at 7:52";
                }

                //the last four departures are not in the usual 15 minutes period
                if(minuteDifference(StringToDate(now), last)<0){
                    return "Last 3 departures are at: 19:32, 20:12, 21:05";
                }

                x= checkTime(busS, now);
            }

            //St Mary's -->> Ealing Broadway
            if(direction==2){
                busS = "07:50";
                first = StringToDate(busS);
                last = StringToDate("18:35");

                if(minuteDifference(StringToDate(now), first)>0){
                    return "First departure is at 7:50";
                }
                //the last four departures are not in the usual 15 minutes period
                if(minuteDifference(StringToDate(now), last)<0){
                    return "Last four departures are at: 19:15, 19:55, 20:35, 21:55";
                }

                x= checkTime(busS, now);
            }
        }

        //Bond -->> Ealing Broadway
        if(station==5){
            //The station in Bond street only goes to Ealing Broadway.
            busS = "07:52";
            first = StringToDate(busS);
            last = StringToDate("18:37");

            if(minuteDifference(StringToDate(now), first)>0){
                return "First departure is at 7:52";
            }
            //the last four departures are not in the usual 15 minutes period
            if(minuteDifference(StringToDate(now), last)<0){
                return "Last four departures are at: 19:17, 19:57, 20:37, 21:57";
            }

            x= checkTime(busS, now);
        }

        if(station==6){
            //The station in High St. only goes to Paragon.
            busS = "07:50";
            first = StringToDate(busS);
            last = StringToDate("18:50");

            if(minuteDifference(StringToDate(now), first)>0){
                return "First departure is at 7:50";
            }

            //the last four departures are not in the usual 15 minutes period
            if(minuteDifference(StringToDate(now), last)<0){
                return "Last 3 departures are at: 19:30, 20:10, 21:03";
            }

            x= checkTime(busS, now);
        }

        if(station==7){
            // Ealing can only go towards Paragon
            busS = "07:47";
            first = StringToDate(busS);
            last = StringToDate("18:47");

            if(minuteDifference(StringToDate(now), first)>0){
                return "First departure is at 7:47";
            }

            //the last four departures are not in the usual 15 minutes period
            if(minuteDifference(StringToDate(now), last)<0){
                return "Last 3 departures are at: 19:27, 20:07, 21:00";
            }

            x= checkTime(busS, now);
        }

        if(x>15 || x<0) return errorCode(x);
        return Integer.toString(x);
    }



    public int checkTime(String busS, String now){
        while(true){
            try {
                bus = df.parse(busS);
            } catch (ParseException e) {
                return -1;
            }
            x = minuteDifference(StringToDate(now), bus);
            if(x>0){
                return x;
            }else {
                Calendar cal = Calendar.getInstance();
                cal.setTime(bus);
                cal.add(Calendar.MINUTE, 15);
                busS = df.format(cal.getTime());
            }
        }



    }




    public Date StringToDate(final String date)
    {
        final Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        final SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        format.setCalendar(cal);

        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }



    public int minuteDifference(Date earlierDate, Date laterDate)
    {
        if( earlierDate == null || laterDate == null ) return -1;

        return (int)((laterDate.getTime()/60000) - (earlierDate.getTime()/60000));
    }



    public String errorCode(int x){
        if(x==-1) return "ERROR:\n Please send a screenshot to alexandru.t@tech-center.com \n Error code StringToDate/"+Long.toString(x)+"/"+station+"/"+direction;
        if(x==-2) return "ERROR:\n Please send a screenshot to alexandru.t@tech-center.com \n Error code emptyMethod/"+Long.toString(x)+"/"+station+"/"+direction;
        if(x>15 || x<0)  return "ERROR:\n Please send a screenshot to alexandru.t@tech-center.com \n Error code WrongValues/"+Long.toString(x)+"/"+station+"/"+direction;

        return "ERROR:\n Please send a screenshot to alexandru.t@tech-center.com \n Error code ?/"+Long.toString(x)+"/"+station+"/"+direction;
    }
}
