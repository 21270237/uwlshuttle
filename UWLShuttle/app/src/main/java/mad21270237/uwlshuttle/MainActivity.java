package mad21270237.uwlshuttle;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{


    private int station=0, destination=0, vis=0, vis1=0, oneWay;
    private SeekBar seekBar;
    private TextView value;
    private ImageButton conf, minus2, plus2;
    private LinearLayout line, line1;
    private SeekBar seekBar2;
    private TextView value2;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toast.makeText(getApplicationContext(),
                " Please note that you need to have a 24 hours time setting.", Toast.LENGTH_LONG)
                .show();
        line = (LinearLayout) findViewById(R.id.layout);
        line1 = (LinearLayout) findViewById(R.id.layout1);
        conf = (ImageButton) findViewById(R.id.confirm);
        minus2 = (ImageButton)findViewById(R.id.minus2);
        plus2 = (ImageButton)findViewById(R.id.plus2);
        value = (TextView) findViewById(R.id.value);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        ImageButton plus = (ImageButton) findViewById(R.id.plus);
        ImageButton minus = (ImageButton) findViewById(R.id.minus);
        seekBar.setOnSeekBarChangeListener(this);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar.setProgress(seekBar.getProgress() + 1);
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar.setProgress(seekBar.getProgress() - 1);
            }
        });


        value2 = (TextView) findViewById(R.id.value2);
        seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
        ImageButton plus2 = (ImageButton) findViewById(R.id.plus2);
        ImageButton minus2 = (ImageButton) findViewById(R.id.minus2);
        seekBar2.setOnSeekBarChangeListener(this);
        seekBar2.setEnabled(false);
        plus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar2.setProgress((seekBar2.getProgress() + 1));
            }
        });

        minus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar2.setProgress(seekBar2.getProgress() - 1);
            }
        });

        line1.setBackgroundResource(R.drawable.bus);

        Calendar cal = Calendar.getInstance();
        int day = cal.get(cal.DAY_OF_WEEK);
        if(day==1 || day ==7){
            line.setBackgroundResource(R.drawable.weekend);
            line1.setBackgroundResource(R.drawable.weekend);
            seekBar.setEnabled(false);
            plus.setVisibility(View.INVISIBLE);
            minus.setVisibility(View.INVISIBLE);
            value.setText("");
        }


    }

    @Override
    public void onProgressChanged(SeekBar seek, int progress, boolean fromTouch) {
        switch (seek.getId()) {
            case R.id.seekBar:
                station=progress;
                topDisplay();
                break;

            case R.id.seekBar2:
                destination=progress;
                bottomDisplay();
                break;

        }

        setVisuals();
        Holder.setStation(station);
        Holder.setDirection(station,destination);

    }




    @Override
    public void onStartTrackingTouch(SeekBar arg0) {}
    @Override
    public void onStopTrackingTouch(SeekBar arg0) {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contact) {
            startActivity(new Intent(MainActivity.this, Contact.class));
        }

        return super.onOptionsItemSelected(item);
    }


    public void confirm(View v){
        startActivity(new Intent(MainActivity.this, Display.class));
    }

    public void topDisplay(){
        seekBar2.setMax(7);
        seekBar2.setProgress(0);
        oneWay=0;
        line1.setBackgroundResource(R.drawable.bus);
        if(station==0){
            seekBar2.setEnabled(false);
            plus2.setVisibility(View.INVISIBLE);
            minus2.setVisibility(View.INVISIBLE);
            value2.setVisibility(View.INVISIBLE);
            line.setBackgroundResource(R.drawable.bu2);
            vis=0;
            value.setText(getApplicationContext().getResources().getString(R.string.station));
        }else{
            seekBar2.setEnabled(true);
            plus2.setVisibility(View.VISIBLE);
            minus2.setVisibility(View.VISIBLE);
            value2.setVisibility(View.VISIBLE);
            vis=1;
            oneWay=-1;
        }
        if(station==1){
            line.setBackgroundResource(R.drawable.paragon);
            value.setText(getApplicationContext().getResources().getString(R.string.sP));
        }
        if(station==2){
            value.setText(getApplicationContext().getResources().getString(R.string.sL));
            line.setBackgroundResource(R.drawable.little);
        }
        if(station==3){
            value.setText(getApplicationContext().getResources().getString(R.string.sSo));
            line.setBackgroundResource(R.drawable.south);
        }
        if(station==4){
            line.setBackgroundResource(R.drawable.stmary);
            value.setText(getApplicationContext().getResources().getString(R.string.sSt));

        }
        if(station==5){
            value.setText(getApplicationContext().getResources().getString(R.string.sB));
            line.setBackgroundResource(R.drawable.bond);
            seekBar2.setProgress(7);
            seekBar2.setEnabled(false);
            plus2.setVisibility(View.INVISIBLE);
            minus2.setVisibility(View.INVISIBLE);
        }
        if(station==6){
            value.setText(getApplicationContext().getResources().getString(R.string.sH));
            line.setBackgroundResource(R.drawable.high);
            seekBar2.setMax(4);
        }
        if(station==7){
            value.setText(getApplicationContext().getResources().getString(R.string.sE));
            line.setBackgroundResource(R.drawable.ealing);
            oneWay=1;
        }
    }

    public void bottomDisplay(){
        if(destination==0){
            vis1=0;
            value2.setText(getApplicationContext().getResources().getString(R.string.direction));
            line1.setBackgroundResource(R.drawable.bus);
        }else{
            vis1=1;
        }
        if(destination==1){
            value2.setText(getApplicationContext().getResources().getString(R.string.sP));
            line1.setBackgroundResource(R.drawable.paragon);
        }
        if(destination==2){
            value2.setText(getApplicationContext().getResources().getString(R.string.sL));
            line1.setBackgroundResource(R.drawable.little);
        }
        if(destination==3){
            value2.setText(getApplicationContext().getResources().getString(R.string.sSo));
            line1.setBackgroundResource(R.drawable.south);
        }
        if(destination==4){
            value2.setText(getApplicationContext().getResources().getString(R.string.sSt));
            line1.setBackgroundResource(R.drawable.stmary);
        }
        if(destination == 5) {
            if(oneWay==1){
                value2.setText(getApplicationContext().getResources().getString(R.string.sH));
                line1.setBackgroundResource(R.drawable.high);
                seekBar2.setMax(5);
            }else{
                value2.setText(getApplicationContext().getResources().getString(R.string.sB));
                line1.setBackgroundResource(R.drawable.bond);
            }}
        if(destination==6){
            if(oneWay==-1){
                value2.setText(getApplicationContext().getResources().getString(R.string.sE));
                line1.setBackgroundResource(R.drawable.ealing);
                seekBar2.setMax(6);

            }else{
                value2.setText(getApplicationContext().getResources().getString(R.string.sH));
                line1.setBackgroundResource(R.drawable.high);
            }
        }
        if(destination==7){
            value2.setText(getApplicationContext().getResources().getString(R.string.sE));
            line1.setBackgroundResource(R.drawable.ealing);

        }
    }

    public void setVisuals(){
        if(vis==0 || vis1==0 || station==destination) {
            conf.setVisibility(View.INVISIBLE);
        }else{
            conf.setVisibility(View.VISIBLE);
        }
    }




}

